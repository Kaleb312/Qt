#include <QApplication>
#include "MyWindow.h"


int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Password *pass = new Password;
    pass->show();

    MyWindow *window = new MyWindow();

    //window->show();

    Str *n = new Str;

    QObject::connect(pass, SIGNAL(rightPass()), window, SLOT(show()));
    QObject::connect(window, SIGNAL(simple(QString)), n, SLOT(Simple(QString)));
    QObject::connect(window, SIGNAL(both(QString)), n, SLOT(Both(QString)));
    QObject::connect(window, SIGNAL(inver(QString)), n, SLOT(Inversia(QString)));
    QObject::connect(window, SIGNAL(upReg(QString)), n, SLOT(UpperReg(QString)));






    return app.exec();
}
