#ifndef MYWINDOW_H
#define MYWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QMessageBox>

class MyWindow : public QDialog
{
    Q_OBJECT

public:
    MyWindow(QWidget *parent = 0);
private:
    QLabel *lbl;
    QLineEdit *line;
    QCheckBox *cb1;
    QCheckBox *cb2;
    QPushButton *ok;
    QPushButton *close;
private slots:
    void OkClicked();
    void TextChenged(QString str);
signals:
    void upReg (QString str);
    void inver (QString str);
    void simple (QString str);
    void both (QString str);
};

class Str : public QObject
{
    Q_OBJECT
public slots:
    void Simple(QString str)
    {
        QMessageBox msg;
        msg.setText(str);
        msg.exec();
    }
    void Inversia(QString str)
    {
        QString result;
        for (int i=str.size()-1, j = 0; i >=0; i--, j++)
            result[j] = str[i];
        QMessageBox msg;
        msg.setText(result);
        msg.exec();
    }
    void UpperReg(QString str)
    {
        QString result;
        for (int i = 0; i<=str.size(); i++)
        {
            if((str[i]).isLower())
                result[i] = (str[i]).toUpper();
            else
                result[i] = str[i];
        }
        QMessageBox msg;
        msg.setText(result);
        msg.exec();
    }
    void Both(QString str)
    {
        QString result;
        str = str.toUpper();
        for (int i=str.size()-1, j = 0; i >=0; i--, j++)
            result[j] = str[i];
        QMessageBox msg;
        msg.setText(result);
        msg.exec();
    }


};

class Password : public QDialog
{
    Q_OBJECT
private:
    QString *pass;
    QLabel *lbl;
    QLineEdit *line;
    QPushButton *ok;
private slots:
    void OkClicked()
    {
        if(line->text() == "123")
        {
            this->close();
            emit rightPass();
        }
        else
        {
            QMessageBox m;
            m.setText("Wrong");
            m.exec();
        }
    }

    void TextChenged(QString str)
    {
        ok->setEnabled(!str.isEmpty());
    }
signals:
    void rightPass();

public:
    Password()
    {
        lbl = new QLabel("&Password");
        line = new QLineEdit();
        lbl->setBuddy(line);

        ok = new QPushButton ("Ok");
        ok->setDefault(true);
        ok->setEnabled(false);

        QHBoxLayout *lay = new QHBoxLayout;
        lay->addWidget(lbl);
        lay->addWidget(line);
        lay->addWidget(ok);

        setLayout(lay);
        setWindowTitle("Password");

        connect(line, SIGNAL(textChanged(QString)), this, SLOT(TextChenged(QString)));
        connect(ok, SIGNAL(clicked()), this, SLOT(OkClicked()));
    }




};

#endif // MYWINDOW_H
