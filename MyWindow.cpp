#include "MyWindow.h"


MyWindow::MyWindow (QWidget *parent) : QDialog(parent)
{
    lbl = new QLabel("&Enter text");
    line = new QLineEdit;
    lbl->setBuddy(line);

    cb1 = new QCheckBox("Upper register");
    cb2 = new QCheckBox("Inversion");

    ok = new QPushButton("Ok");
    ok->setDefault(true);
    ok->setEnabled(false);

    close = new QPushButton("&Close");

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(lbl);
    layout->addWidget(line);

    QVBoxLayout *right = new QVBoxLayout;
    right->addLayout(layout);
    right->addWidget(cb1);
    right->addWidget(cb2);

    QVBoxLayout *left = new QVBoxLayout;
    left->addWidget(ok);
    left->addWidget(close);

    QHBoxLayout *main = new QHBoxLayout;
    main->addLayout(right);
    main->addLayout(left);

    connect(line, SIGNAL(textChanged(QString)), this, SLOT(TextChenged(QString)));
    connect(close, SIGNAL(clicked()), this, SLOT(close()));
    connect(ok, SIGNAL(clicked()), this, SLOT(OkClicked()));

    setLayout(main);
    setWindowTitle("Message Box");
}

void MyWindow::TextChenged(QString str)
{
    ok->setEnabled(!str.isEmpty());
}

void MyWindow::OkClicked()
{
    if (!cb1->isChecked() && !cb2->isChecked())
        emit simple(line->text());
    else if (cb1->isChecked() && cb2->isChecked())
        emit both(line->text());
    else if (cb1->isChecked())
        emit upReg(line->text());
    else if (cb2->isChecked())
        emit inver(line->text());



}
